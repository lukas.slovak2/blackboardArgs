package sk.lukasSlovak.args;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Categories.ExcludeCategory;

public class ArgsTest {
	
	@Test
	public void basicTest() throws ArgumentException{
		String[] arguments = {"-l", "true",  "-p",  "2222", "-d", "/tmp", "-v", "22.5", "-a", "a,b,c"};
		Args args = new Args("l,p#,d*,v##,a[*]", arguments);
		
		boolean logEnabled = args.getValue('l', Boolean.class);
		Assert.assertTrue(logEnabled);
        int portNumber = args.getValue('p', Integer.class);
        Assert.assertEquals(2222, portNumber);
        String path = args.getValue('d', String.class);
        Assert.assertEquals("/tmp", path);
        double value = args.getValue('v', Double.class);
        Assert.assertEquals(22.5, value, 0d);
        String[] lines = args.getValue('a', String[].class);
        Assert.assertEquals(lines[0], "a");
        Assert.assertEquals(lines[1], "b");
        Assert.assertEquals(lines[2], "c");        
	}
	
	@Test(expected = ArgumentException.class)
	public void insufficientArgumentsTest() throws ArgumentException{
		String[] arguments = {"-l", "true",  "-p",  "2222", "-d", "/tmp", "-v", "22.5"};
		Args args = new Args("l,p#,d*,v##,a[*]", arguments);
	}
	
	@Test(expected = ArgumentException.class)
	public void missingParameterValueTest() throws ArgumentException{
		String[] arguments = {"-l"};
		Args args = new Args("l", arguments);
	}
	
	//pattern tests
	@Test(expected = ArgumentException.class)
	public void testEmptyPattern() throws ArgumentException{
		String[] arguments = {};
		Args args = new Args("", arguments);
	}
	
	@Test(expected = ArgumentException.class)
	public void nullPattern() throws ArgumentException{
		String[] arguments = {};
		Args args = new Args(null, arguments);
	}
	
	@Test(expected = ArgumentException.class)
	public void invalidPattern() throws ArgumentException{
		String[] arguments = {};
		Args args = new Args("fdsf", arguments);
	}
	
	//arguments tets
	@Test(expected = ArgumentException.class)
	public void invalidBooleanTest() throws ArgumentException{
		String[] arguments = {"-l", "lalala"};
		Args args = new Args("l", arguments);		
	}
	
	@Test(expected = ArgumentException.class)
	public void invalidIntegerTest() throws ArgumentException{
		String[] arguments = {"-l", "lalala"};
		Args args = new Args("l#", arguments);		
	}
	
	@Test(expected = ArgumentException.class)
	public void invalidDoubleTest() throws ArgumentException{
		String[] arguments = {"-l", "lalala"};
		Args args = new Args("l##", arguments);		
	}
	
	@Test(expected = ArgumentException.class)
	public void invalidStringArrayTest() throws ArgumentException{
		String[] arguments = {"-l", ""};
		Args args = new Args("l[*]", arguments);		
	}
	

}
