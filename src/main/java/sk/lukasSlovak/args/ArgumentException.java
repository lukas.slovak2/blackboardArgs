package sk.lukasSlovak.args;

public class ArgumentException extends Exception{

	private static final long serialVersionUID = -451632542053254163L;
	
	private String errorMessage;
	
	public ArgumentException(String errorMessage){
		this.errorMessage = errorMessage;
	}
	
	public String getErrorMessage(){
		return this.errorMessage;
	}
	
	

}
