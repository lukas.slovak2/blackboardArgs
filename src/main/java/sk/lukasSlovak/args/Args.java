package sk.lukasSlovak.args;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Args {
	
	private static Pattern PATTEN_REGEX = Pattern.compile("^[a-z](##|#|\\[\\*\\]|\\*|)$");
	private static Pattern PARAMETER_REGEX = Pattern.compile("^-[a-z]$");
	
	/**
	 * stores actual parameter values (name -> typed value)
	 */
	private Map<Character, Object> arguments = new HashMap<>();
	/**
	 * stores parameter definition (name -> type)
	 */
	private Map<Character, Class<?>> parameters = new HashMap<>();
	
	public Args(String pattern, String[] args) throws ArgumentException {
		//parameters		
		this.parameters = deriveParameters(pattern);
		//arguments
		this.arguments = parseArguments(args, this.parameters);
	}
	
	/**
	 * tries to read parameter values from input respecting defined parameter scheme
	 * 
	 * @param args
	 * 		given program arguments
	 * @param parameters
	 * 		map of defined parameters with their expected types
	 * @return
	 * @throws ArgumentException
	 */
	private static Map<Character, Object> parseArguments(String[] args, Map<Character, Class<?>> parameters) throws ArgumentException {
		List<String> argsList = Arrays.asList(args);
		Iterator<String> it = argsList.iterator();
		Map<Character, Object> arguments = new HashMap<>();
		while(it.hasNext()){
			String usedparameter = it.next();
			Matcher matcher = PARAMETER_REGEX.matcher(usedparameter);
			if(matcher.find()){
				char parameterName = usedparameter.charAt(1);
				Class<?> parameterType = parameters.get(parameterName);
				Object parameterValue = getTypedParameterValue(it, usedparameter, parameterType, parameterName);
				arguments.put(parameterName, parameterValue);
			} else{
				throw new ArgumentException("Invalid usage of parameter: " + usedparameter);
			}
		}
		
		if(arguments.size() != parameters.size()){
			throw new ArgumentException(String.format("Illegal number of arguments. Expected: %s, actual: %s", parameters.size() * 2, arguments.size() * 2));
		}
		
		return arguments;
	}

	private static Object getTypedParameterValue(Iterator<String> it, String usedparameter, Class<?> parameterType, char parameterName) throws ArgumentException {
		if(parameterType == null){
			throw new ArgumentException("Unknown parameter: " + usedparameter);
		}
		
		if(it.hasNext()){
			String parameterValue = it.next();
			return getTypedParameterValue(parameterType, parameterValue, usedparameter);
		} else {
			throw new ArgumentException("No value provided for parameter: " + usedparameter);
		}
	}
	
	private static Object getTypedParameterValue(Class<?> parameterType, String parameterValue, String usedparameter) throws ArgumentException {
		try{
			if (Boolean.class.equals(parameterType)){
				return parseBoolean(parameterType, parameterValue, usedparameter);
			} else if (Integer.class.equals(parameterType)){
				return Integer.valueOf(parameterValue);
			} else if (Double.class.equals(parameterType)){
				return Double.valueOf(parameterValue);
			} else if (String.class.equals(parameterType)){
				return parameterValue;
			} else if (String[].class.equals(parameterType)){
				return parseStringArray(parameterValue);
			} else {
				throw new ArgumentException(String.format("Unexpected value type '%s' for parameter '%s'", parameterType.getSimpleName(), usedparameter));
			}
		} catch (Exception e){
			throw new ArgumentException(String.format("Value '%s' for parameter '%s' can't be parsed as '%s'", parameterValue, usedparameter, parameterType.getSimpleName()));
		}
		
	}
	
	private static String[] parseStringArray(String parameterValue){
		if(parameterValue == null || parameterValue.isEmpty()){
			throw new IllegalStateException();
		}
		
		return parameterValue.split(",");
	}

	private static Object parseBoolean(Class<?> parameterType, String parameterValue,	String usedparameter) throws ArgumentException {
		if("true".equalsIgnoreCase(parameterValue)){
			return Boolean.TRUE;
		} else if ("false".equalsIgnoreCase(parameterValue)){
			return Boolean.FALSE;
		} else {
			throw new IllegalArgumentException();
		}
	}


	/**
	 * parses pattern and derives parameters that should be used
	 * 
	 * @param pattern
	 * @return
	 * @throws ArgumentException
	 */
	private static Map<Character, Class<?>> deriveParameters(String pattern) throws ArgumentException {
		if (pattern == null || pattern.isEmpty()){
			throw new ArgumentException("Empty pattern provided. Cannot define parameters.");
		}
		String[] patternParts = pattern.split(",");
		Map<Character, Class<?>> parameters = new HashMap<>();
		for(String patternPart: patternParts){
			Matcher matcher = PATTEN_REGEX.matcher(patternPart);
			if(matcher.find()){
				String typeDefinition = matcher.group(1);				
				Class<?> parameterType = getType(typeDefinition);
				parameters.put(patternPart.charAt(0), parameterType);
			} else {
				throw new ArgumentException("Unexpected parameter definition " + patternPart);
			}
		}
		return parameters;
	}


	private static Class<?> getType(String typeDefinition) throws ArgumentException {
		Class<?> parameterType;
		switch(typeDefinition){
			case "":
				parameterType = Boolean.class;
				break;
			case "#":
				parameterType = Integer.class;
				break;
			case "##":
				parameterType = Double.class;
				break;
			case "*":
				parameterType = String.class;
				break;
			case "[*]":
				parameterType = String[].class;
				break;
			default:
				throw new ArgumentException("Unknown parameter type " + typeDefinition);
		}
		return parameterType;
	}

	
	public <T> T getValue(char parameterName, Class<T> clazz) throws ArgumentException {
		Class<?> parameterType = this.parameters.get(parameterName);
		if (parameterType == null){
			throw new ArgumentException("Unknown parameter: " + parameterName);
		}
		if (clazz == null) {
			throw new ArgumentException("Value of parameter cannot be of class 'null'");
		}
		if (parameterType.equals(clazz)){
			return clazz.cast(this.arguments.get(parameterName));
		}else {
			throw new ArgumentException(String.format("parameter '%s' is of type '%s', not '%s'", parameterName, parameterType.getSimpleName(), clazz.getSimpleName()));
		}
	}
	
	//example usage
	public static void main(String[] args) {
		try {
            Args ar = new Args("l,p#,d*", args);
            System.out.println(ar.getValue('l', Boolean.class));
            System.out.println(ar.getValue('p', Integer.class));
            System.out.println(ar.getValue('d', String.class));
		} catch (ArgumentException e) {
			System.out.printf("Argument error: %s\n", e.getErrorMessage());
			e.printStackTrace();
		}
	}
}
